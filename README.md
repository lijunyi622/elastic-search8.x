# elasticSearch8.x

#### 介绍
 **elasticSearch8.x（v8.3.3）最新版的笔记记录** 

 **elasticSearch系列笔记博客直达** ：[elasticSearch](https://lijunyi.xyz/docs/middleware/elasticSearch/abstract.html)

#### 学习教程

1.  clone项目，根据笔记内容进行一步步学习深入

### github 仓库
[elastic-search8.x](https://github.com/LiJunYi2/elastic-search8.x)

